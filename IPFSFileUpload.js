var fs = require("fs");
const IPFS = require('ipfs-api');
const ipfs = new IPFS({
	host: 'ipfs.infura.io',
	port: 5001,
	protocol: 'https'
});
async function UploadFile() {
	const files = [{
		path: './myfile.md',
		content: await fs.readFileSync('./myfile.md')
	}]

	await ipfs.add(files, (err, ipfsHash) => {
		console.log("---2-------errr---", err)
		console.log("--2--ipfsHash---", ipfsHash)
	});
}
UploadFile();