const ethers = require('ethers'); 
let provider = ethers.getDefaultProvider('ropsten');  
const abiDecoder = require('abi-decoder'); // NodeJS

/*console.log("---1---",provider)*/
let privateKey = "0x12676683BFC69B3AF8F3ED197B32A8E08E68DD8992B56A1EDDB4ED03CC00E511";
let wallet = new  ethers.Wallet(privateKey ,provider);
const IPFSABI=[
	{
		"constant": true,
		"inputs": [],
		"name": "status",
		"outputs": [
			{
				"name": "",
				"type": "bool"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_verifieradd",
				"type": "address"
			},
			{
				"name": "_verifiername",
				"type": "string"
			}
		],
		"name": "addVerifier",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "_isverifier",
				"type": "address"
			}
		],
		"name": "checkDocumentVerifier",
		"outputs": [
			{
				"name": "",
				"type": "bool"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_hash",
				"type": "string"
			},
			{
				"name": "_add",
				"type": "address"
			}
		],
		"name": "getDocumentbyHashAddress",
		"outputs": [
			{
				"components": [
					{
						"name": "owner",
						"type": "address"
					},
					{
						"name": "hash",
						"type": "string"
					},
					{
						"name": "status",
						"type": "string"
					}
				],
				"name": "",
				"type": "tuple"
			}
		],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [],
		"name": "testverifier",
		"outputs": [
			{
				"name": "",
				"type": "bool"
			}
		],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_add",
				"type": "address"
			},
			{
				"name": "_hash",
				"type": "string"
			},
			{
				"name": "_status",
				"type": "string"
			}
		],
		"name": "addDocumentUser",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_user",
				"type": "address"
			},
			{
				"name": "_hash",
				"type": "string"
			},
			{
				"name": "_status",
				"type": "string"
			}
		],
		"name": "updateDocumentStatus",
		"outputs": [
			{
				"components": [
					{
						"name": "owner",
						"type": "address"
					},
					{
						"name": "hash",
						"type": "string"
					},
					{
						"name": "status",
						"type": "string"
					}
				],
				"name": "",
				"type": "tuple"
			}
		],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_addr",
				"type": "address"
			}
		],
		"name": "getDocumentList",
		"outputs": [
			{
				"components": [
					{
						"name": "owner",
						"type": "address"
					},
					{
						"name": "hash",
						"type": "string"
					},
					{
						"name": "status",
						"type": "string"
					}
				],
				"name": "DocList",
				"type": "tuple[]"
			}
		],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_verifieradd",
				"type": "address"
			}
		],
		"name": "removeVerifier",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "constructor"
	}
];
abiDecoder.addABI(IPFSABI); 

let contractAddress = "0x1e46a7dc55e570a8cfea2ad1c28b6901df91b67a";
let contract = new ethers.Contract(contractAddress, IPFSABI, provider);
// console.log("-----2------contract",contract)
let contractInstance = contract.connect(wallet); 
// console.log("----3------,contractInstance",contractInstance)

async  function addUserDocument () { 
 	 let tx = await contractInstance.addDocumentUser("0x1e46a7dc55e570a8cfea2ad1c28b6901df91b67a","IPFS HASH","pending status");  
 	// let tx = await contractInstance.getValue();
 	console.log("----4-----",tx)

 } 
async function getUserDocument(){
	 let info = await contractInstance.getDocumentbyHashAddress("IPFS HASH","0x1e46a7dc55e570a8cfea2ad1c28b6901df91b67a");
	 // console.log("--2---,info",info.data)
	 const Data = info.data;
    const decodedData = abiDecoder.decodeMethod(Data); 
    console.log("decodedData",decodedData)
}
  

async function UpdateUserDocumentStatus(){
	 let info = await contractInstance.updateDocumentStatus("0x1e46a7dc55e570a8cfea2ad1c28b6901df91b67a","IPFS HASH","accpeted");
	 // console.log("--2---,info",info.data)
	 const Data = info.data;
    const decodedData = abiDecoder.decodeMethod(Data); 
    console.log("decodedData",decodedData)
}
  
 async function addVerifier(){  
 	console.log("Insise Verifier")
 	 let info = await contractInstance.addVerifier("0x1e46a7dc55e570a8cfea2ad1c28b6901df91b67a","Verifier");
	 // console.log("--2---,info",info.data)
	 const Data = info.data;
    const decodedData = abiDecoder.decodeMethod(Data); 
    console.log("decodedData",decodedData)

 } 
async function removeVerifier(){ 
 	 let info = await contractInstance.removeVerifier("0x1e46a7dc55e570a8cfea2ad1c28b6901df91b67a");
	 // console.log("--2---,info",info.data)
	 const Data = info.data;
    const decodedData = abiDecoder.decodeMethod(Data); 
    console.log("decodedData",decodedData)

 } 

addVerifier();
 //getUserDocument();
// addUserDocument();