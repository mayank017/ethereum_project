const openpgp = require('openpgp') // use as CommonJS, AMD, ES6 module or via window.openpgp
var fs = require("fs");
 openpgp.initWorker({ path:'openpgp.worker.js' }) // set the relative web worker path
openpgp.config.show_version = false;
openpgp.config.show_comment = false;
var options = {
    userIds: [{ name:'Jon Smith', email:'jon@example.com' }], // multiple user IDs
    curve: "ed25519",                                         // ECC curve name
    passphrase: 'super long and hard to guess secret'         // protects the private key
};
 var pubkey,privkey;
const passphrase = 'super long and hard to guess secret' //what the privKey is encrypted with
openpgp.generateKey(options).then( async function(key) {
    privkey = key.privateKeyArmored; // '-----BEGIN PGP PRIVATE KEY BLOCK ... '
     pubkey = key.publicKeyArmored;   // '-----BEGIN PGP PUBLIC KEY BLOCK ... '
    var revocationCertificate = key.revocationCertificate; // '-----BEGIN PGP PUBLIC KEY BLOCK ... '  
await fs.writeFileSync('./privkey',privkey);
await fs.writeFileSync('./pubkey',pubkey);
   /* encryptDecryptFunction() */

/*    encryption();
*/ 

  // decryption();
}); 



const encryptDecryptFunction = async() => {
    const privKeyObj = (await openpgp.key.readArmored(privkey)).keys[0]
    await privKeyObj.decrypt(passphrase)

    const options = {
        message: openpgp.message.fromText('Hello, World!'),       // input as Message object
        publicKeys: (await openpgp.key.readArmored(pubkey)).keys, // for encryption
        privateKeys: [privKeyObj]                                 // for signing (optional)
    }

    openpgp.encrypt(options).then(ciphertext => {
        encrypted = ciphertext.data // '-----BEGIN PGP MESSAGE ... END PGP MESSAGE-----'
        fs.writeFileSync('/home/mayank/Desktop/Blockchain/Smart Contract/Encryption/encrypted', encrypted);
        return encrypted
    })
    .then(async encrypted => { 
/*    var text = fs.readFileSync('test.md','utf8')  
*/    fs.writeFileSync('/home/mayank/Desktop/Blockchain/Smart Contract/Encryption/message',   JSON.stringify( openpgp));
    	await openpgp.message.readArmored(encrypted)
        const options = {
            message: await openpgp.message.readArmored(encrypted),    // parse armored message
            publicKeys: (await openpgp.key.readArmored(pubkey)).keys, // for verification (optional)
            privateKeys: [privKeyObj]                                 // for decryption
        }
        console.log("privatekey object ",privKeyObj)
        openpgp.decrypt(options).then(plaintext => {
            console.log(plaintext.data)
            return plaintext.data // 'Hello, World!'
        })

    }) 
}

async function encryption() {
 	// body...
const privKeyObj = (await openpgp.key.readArmored(privkey)).keys[0] 
    fs.writeFileSync('/home/mayank/Desktop/Blockchain/Smart Contract/Encryption/privKeyObj',  JSON.stringify(privKeyObj));
  await  fs.writeFileSync('/home/mayank/Desktop/Blockchain/Smart Contract/Encryption/publicKeys',  JSON.stringify((await openpgp.key.readArmored(pubkey)).keys));

    await privKeyObj.decrypt(passphrase)

    const options = {
        message: openpgp.message.fromText('Hello, World!'),       // input as Message object
        publicKeys: (await openpgp.key.readArmored(pubkey)).keys, // for encryption
        privateKeys: [privKeyObj]                                 // for signing (optional)
    }

    openpgp.encrypt(options).then(ciphertext => {
        encrypted = ciphertext.data // '-----BEGIN PGP MESSAGE ... END PGP MESSAGE-----'
        fs.writeFileSync('/home/mayank/Desktop/Blockchain/Smart Contract/Encryption/encrypted', encrypted);
        return encrypted;
 } )
}

async function decryption (){  
	const encryptedData = fs.readFileSync('/home/mayank/Desktop/Blockchain/Smart Contract/Encryption/encrypted', 'utf8')
 	    encryptedMessage = openpgp.message.readArmored(encryptedData)  
 	      var privatekey=  fs.readFileSync('/home/mayank/Desktop/Blockchain/Smart Contract/Encryption/privKeyObj',  JSON.stringify(privKeyObj));
 	      var	privKeyObj=JSON.parse(privatekey);
 	      console.log(privKeyObj) 

 	        await privKeyObj.decrypt(passphrase)

openpgp.decrypt({
    message: encryptedMessage,
    publicKeys :decryptPUBKEY,
    privateKeys : [privKeyObj]   
}).then( (decrypted) => { fs.writeFile('/home/mayank/Desktop/Blockchain/Smart Contract/Encryption/decrypted', decrypted.data )} )
}


